package com.michael.recyclerlist.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.michael.recyclerlist.R;

/**
 * Created by michael on 03.05.16.
 *
 * RecyclerView не создает новый layout для каждого элемента, он берет и
 * переписывает единожды созданную переменную. Экономит память.
 * А listview, к слову создает layout под каждый пункт
 */
public class MyRecyclerViewAdapter
        extends RecyclerView.Adapter<MyRecyclerViewAdapter.ItemHolder>{

    String[] strings; //данные
    Context context;

    /*конструктор*/
    public MyRecyclerViewAdapter(String[] strings, Context context) {
        this.strings = strings;
        this.context = context;
    }

    /*берем наш XML, который будет представлять нашу раскладку*/
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent,false);

        return new ItemHolder(view);
    }

    /*здесь мы инициализируем переменные нашей раскладки нашими данными*/
    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        holder.itemText.setText(strings[position]);
        //для recyclerList из-за его повторного использования одной view-хи
        if (position == 3) { //если как-то изменяем одтельный элемент (например, скрываем)
            holder.itemText.setVisibility(View.GONE);
        } else {    //обязательно делаем антидействие для других. иначе они тоже будут GONE
            holder.itemText.setVisibility(View.VISIBLE);
        }

        //notifyDataSetChanged();
        //notifyItemInserted(6,12);
    }

    /*возвращает кол-во элементов списка*/
    @Override
    public int getItemCount() {
        return strings == null ? 0 : strings.length;
    }

    /*держатель для представлений*/
    class ItemHolder extends RecyclerView.ViewHolder {
        TextView itemText;

        public ItemHolder(View itemView) {
            super(itemView);
            initViews(itemView);
        }

        private void initViews(View itemView){
            itemText = (TextView) itemView.findViewById(R.id.item_text);
        }
    }
}
