package com.michael.recyclerlist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.michael.recyclerlist.R;

/**
 * Created by michael on 27.04.16.
 */
public class MyListViewAdapter extends BaseAdapter {
    String[] items;
    Context context;
    LayoutInflater inflater;

    /*конструктор*/
    public MyListViewAdapter(String[] items, Context context) {
        this.items = items;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

    }

    /*возвращает кол-во элементов в адаптере*/
    @Override
    public int getCount() {
        return items == null ? 0 : items.length;
    }

    /*возвращает элемент списка*/
    @Override
    public Object getItem(int position) {
        return items[position];
    }

    /*возвращает id элемента списка*/
    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null){
            view = inflater.inflate(R.layout.list_item, parent, false);
            //parent - где отображаем, false - к root не присоединяем
        }

        //назначим текст элементу списка
        TextView itemText = (TextView) view.findViewById(R.id.item_text);
        itemText.setText(items[position]);

        return view;
    }
}

