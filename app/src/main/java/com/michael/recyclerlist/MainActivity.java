package com.michael.recyclerlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.ListView;
import android.support.v7.widget.RecyclerView;


import com.michael.recyclerlist.adapters.MyListViewAdapter;
import com.michael.recyclerlist.adapters.MyRecyclerViewAdapter;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ListView listView, listViewMyAdapter;
    String[] cats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initData();
        initList();
    }

    private void initViews(){
        listView = (ListView) findViewById(R.id.list_veiw);
        listViewMyAdapter = (ListView) findViewById(R.id.list_veiw_myadapter);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void initData(){
        cats = new String[] {"Васька", "Алиска", "Чубайс", "Ворюга", "Максимилиан", "Стефания", "Ромка", "Малевич"};
    }

    private void initList(){
        /*

        //сделаем адаптер на всем стандартном
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,cats);
        //привяжем его к нашему списку
        listView.setAdapter(adapter);

        */


        /*

        //воспользуемся своим, собственноручно написанным адаптером
        MyListViewAdapter adapter2 = new MyListViewAdapter(cats,getApplicationContext());
        listViewMyAdapter.setAdapter(adapter2);

        */

        //создали адаптер под recyclerview
        MyRecyclerViewAdapter adapter3 = new MyRecyclerViewAdapter(cats, this);

        //к recyclerview присоединили менеджер раскладок, который позволяет создавать
        //как вертикальные, так и горизональные списки
        recyclerView
                .setLayoutManager(new LinearLayoutManager(
                        this,
                        LinearLayoutManager.VERTICAL, false
                ));

        //к созданному recyclerView присоединили адаптер
        recyclerView.setAdapter(adapter3);
    }

}
